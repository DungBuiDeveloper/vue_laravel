require("./bootstrap");

window.Vue = require("vue");

import VueRouter from "vue-router";
Vue.use(VueRouter);

import VueAxios from "vue-axios";
import axios from "axios";
import _ from "lodash";
import App from "./App.vue";
Vue.use(VueAxios, axios);
Vue.component("todo-item", TodoTemplate);
import HomeComponent from "./components/HomeComponent.vue";
import TodoTemplate from "./components/TodoTemplate.vue";
import CreateComponent from "./components/CreateComponent.vue";
import IndexComponent from "./components/IndexComponent.vue";
import EditComponent from "./components/EditComponent.vue";
import ExampleComponent from "./components/ExampleComponent.vue";

const routes = [
    {
        name: "home",
        path: "/",
        component: HomeComponent
    },
    {
        name: "example",
        path: "/example",
        component: ExampleComponent
    },
    {
        name: "create",
        path: "/create",
        component: CreateComponent
    },
    {
        name: "posts",
        path: "/posts",
        component: IndexComponent
    },
    {
        name: "edit",
        path: "/edit/:id",
        component: EditComponent
    }
];

const router = new VueRouter({ mode: "history", routes: routes });
const app = new Vue(Vue.util.extend({ router }, App)).$mount("#app");
